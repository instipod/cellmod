package com.instipod.cellmod.commands;

import com.instipod.cellmod.CellMod;
import com.instipod.cellmod.TLogger;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NumberCmd implements CommandExecutor {

    private final CellMod plugin;
    private CommandSender cSender;

    public NumberCmd(CellMod instance) {
        plugin = instance;
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        Player player = (Player) cs;
        if (plugin.permissions.has(player, "cellmod.use.number")) {
        if (strings.length > 0) {
            ResultSet rs = plugin.getResult("SELECT * FROM players WHERE Player='" + player.getName() + "' AND Changed='true';");
        Boolean changed = false;
        try {
            while (rs.next()) {
                changed = true;
            }
        } catch (SQLException ex) {
            TLogger.log(Level.SEVERE, "Failed to read player number for API access!");
        }
        rs = plugin.getResult("SELECT * FROM players WHERE Number='" + strings[0] + "';");
        Boolean inuse = false;
        try {
            while (rs.next()) {
                inuse = true;
            }
        } catch (SQLException ex) {
            TLogger.log(Level.SEVERE, "Failed to read player number for API access!");
        }
        if (changed == false) {
            if (inuse == false) {
            plugin.runUpdateQuery("UPDATE players SET Number='" + strings[0] + "', Changed='true' WHERE Player='" + player.getName() + "';");
            player.sendMessage(ChatColor.GREEN + plugin.lang.getProperty("NumChanged"));
            } else {
                player.sendMessage(ChatColor.RED + plugin.lang.getProperty("NumAlready"));
            }
        } else {
            player.sendMessage(ChatColor.RED + plugin.lang.getProperty("NumNoMore"));
        }
        } else {
            player.sendMessage(ChatColor.RED + plugin.lang.getProperty("NumWrongLength"));
        }
        return true;
    } else {
            player.sendMessage(ChatColor.RED + plugin.lang.getProperty("NoPermission"));
            return true;
        }
    }
}

