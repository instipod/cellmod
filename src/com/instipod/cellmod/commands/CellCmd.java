package com.instipod.cellmod.commands;

import com.instipod.cellmod.CellMod;
import com.instipod.cellmod.TLogger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CellCmd implements CommandExecutor {

    private final CellMod plugin;
    private CommandSender cSender;

    public CellCmd(CellMod instance) {
        plugin = instance;
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        if (strings.length > 0) {
            if ("about".equals(strings[0])) {
                cs.sendMessage("This server is running version " + plugin.version + " of the CellMod plugin by Instipod.");
            }
        } else {
        if (plugin.permissions.has((Player) cs, "cellmod.use.cell")) {
           cs.sendMessage(plugin.lang.getProperty("Header"));
           cs.sendMessage(plugin.config.getProperty("network-name") + " " + plugin.getSignal((Player) cs, 0.0));
           Player player = (Player) cs;
           ResultSet rs = plugin.getResult("SELECT * FROM players WHERE Player='" + player.getName() + "';");
        String number = null;
        try {
            while (rs.next()) {
                number = rs.getString("Number");
            }
        } catch (SQLException ex) {
            TLogger.log(Level.SEVERE, "Failed to read player number!");
        }
           player.sendMessage(plugin.lang.getProperty("NumberIs") + " " + number);
           cs.sendMessage(plugin.lang.getProperty("TypeSend"));
        } else {
            cs.sendMessage(ChatColor.RED + plugin.lang.getProperty("NoPermission"));
        }
        
    }
        return true;
    }
}
